import 'package:cawis/app.dart';
import 'package:cawis/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  // initialise the service and bindding widget
  // before application running

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  // run teh whole app
  runApp(const CawisApp());
}
