import 'package:flutter/material.dart';

// init the system color
// bith for dark and light color

// light colors
const lightBackgroundColor = Color(0xFFFFFFFF);
const lightPrimaryColor = Color(0xFF7862FF);
const lightPrimaryTextColor = Color(0xFF282544);
const lightDividerColor = Color(0xFFF0F0F0);
const lightHintColor = Color(0xFF9C9BAB);
const lightLabelColor = Color(0xFF57575C);
const lightSecondaryTextColor = Color(0xFF706D92);
const lightSecondaryColor = Color(0xFFFEBF75);
const ligthSuccessColor = Color(0xFF82D399);
const lightDangerColor = Color(0xFFFC6C6C);
const lightButtonColor = Color(0xFFFFFFFF);

// dark colors
const darkBackgroundColor = Color(0xFF131123);
const darkPrimaryColor = Color(0xFF6A56E4);
const darkPrimaryTextColor = Color(0xFFE6E4F5);
const darkDividerColor = Color(0xFF383550);
const darkHintColor = Color(0xFF686485);
const darkLabelColor = Color(0xFFC6C5D3);
const darkSecondaryTextColor = Color(0xFFB2AED6);
const darkSecondaryColor = Color(0xFFD19957);
const darkSuccessColor = Color(0xFF5FB577);
const darkDangerColor = Color(0xFFC93650);
const darkButtonColor = Color(0xFFE4DFFF);
