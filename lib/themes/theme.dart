import 'package:cawis/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

// main theme
class CawisTheme {
  static ThemeData light() {
    // before we return the theme
    // we need to ensure that system ui chrome
    // setted
    if (!Get.isPlatformDarkMode) {
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(
          systemNavigationBarColor: lightPrimaryTextColor,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarIconBrightness: Brightness.dark,
          statusBarBrightness: Brightness.light,
          statusBarColor: Colors.transparent,
        ),
      );
    }

    // theme data
    // all of teh theme
    return ThemeData(
      brightness: Brightness.light,
      primaryColor: lightPrimaryColor,
      backgroundColor: lightBackgroundColor,
      scaffoldBackgroundColor: lightBackgroundColor,
      hintColor: lightHintColor,
      focusColor: lightPrimaryColor,
      dividerColor: lightDividerColor,
      primaryColorDark: lightPrimaryColor,
      colorScheme: const ColorScheme.light(
        primary: lightPrimaryColor,
        error: Colors.red,
      ),
      fontFamily: GoogleFonts.dmSans().fontFamily,
      appBarTheme: AppBarTheme(
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: true,
        shadowColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: lightPrimaryTextColor,
          size: 28.0,
        ),
        titleTextStyle: TextStyle(
          fontSize: 16.0,
          color: lightPrimaryTextColor,
          fontFamily: GoogleFonts.dmSans().fontFamily,
          fontWeight: FontWeight.w700,
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(
              (states) => lightPrimaryTextColor),
          side: MaterialStateProperty.resolveWith(
            (states) => const BorderSide(
              width: 2.0,
              color: lightPrimaryTextColor,
            ),
          ),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.dmSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith(
              (states) => lightPrimaryTextColor.withOpacity(0.3)),
          elevation: MaterialStateProperty.resolveWith((states) => 0),
          fixedSize: MaterialStateProperty.resolveWith(
            (states) => const Size.fromHeight(52.0),
          ),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith(
              (states) => lightPrimaryTextColor),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.dmSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          foregroundColor: MaterialStateProperty.resolveWith(
              (states) => lightPrimaryTextColor),
          elevation: MaterialStateProperty.resolveWith((states) => 0),
          fixedSize: MaterialStateProperty.resolveWith(
            (states) => const Size.fromHeight(52.0),
          ),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(
              (states) => const Color(0xFF0A0913)),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.dmSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith(
              (states) => const Color(0xFF0A0913).withOpacity(0.4)),
        ),
      ),
      dialogTheme: DialogTheme(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
        elevation: 0.0,
        titleTextStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w700,
          fontSize: 18.0,
        ),
        contentTextStyle: GoogleFonts.dmSans().copyWith(
          height: 1.4,
        ),
      ),
      chipTheme: ChipThemeData(
        backgroundColor: lightPrimaryTextColor,
        disabledColor: Colors.grey[300]!,
        selectedColor: Colors.white,
        secondarySelectedColor: Colors.white30,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
        labelStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w600,
        ),
        secondaryLabelStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w600,
        ),
        brightness: Brightness.light,
      ),
      iconTheme: const IconThemeData(
        size: 20.0,
        color: lightPrimaryTextColor,
      ),
      radioTheme: RadioThemeData(
        fillColor: MaterialStateProperty.resolveWith(
            (states) => lightPrimaryTextColor),
      ),
      tabBarTheme: TabBarTheme(
        labelStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w600,
          fontSize: 13.0,
        ),
        unselectedLabelStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w600,
          fontSize: 13.0,
        ),
        indicator: BoxDecoration(
          color: lightPrimaryTextColor,
          borderRadius: BorderRadius.circular(16.0),
        ),
        labelColor: Colors.white,
        unselectedLabelColor: lightPrimaryTextColor,
      ),
      textTheme: TextTheme(
        headline3: GoogleFonts.dmSans().copyWith(
          color: lightPrimaryTextColor,
        ),
        headline5: GoogleFonts.dmSans().copyWith(
          color: lightPrimaryTextColor,
          fontWeight: FontWeight.bold,
        ),
        headline6: GoogleFonts.dmSans().copyWith(
          color: lightPrimaryTextColor,
        ),
        subtitle1: GoogleFonts.dmSans().copyWith(
          color: lightPrimaryTextColor,
        ),
        bodyText1: GoogleFonts.dmSans().copyWith(
          color: lightPrimaryTextColor,
        ),
        bodyText2: GoogleFonts.dmSans().copyWith(
          color: lightPrimaryTextColor,
        ),
        button: GoogleFonts.dmSans().copyWith(
          color: lightPrimaryTextColor,
        ),
        caption: GoogleFonts.dmSans().copyWith(
          color: lightPrimaryTextColor,
        ),
      ),
    );
  }

  static ThemeData dark() {
    // before we return the theme
    // we need to ensure that system ui chrome
    // setted
    if (Get.isPlatformDarkMode) {
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(
          systemNavigationBarColor: darkPrimaryTextColor,
          statusBarIconBrightness: Brightness.light,
          systemNavigationBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.light,
          statusBarColor: Colors.transparent,
        ),
      );
    }

    // theme data
    // all of teh theme
    return ThemeData(
      brightness: Brightness.dark,
      primaryColor: darkPrimaryTextColor,
      backgroundColor: darkPrimaryTextColor,
      scaffoldBackgroundColor: darkBackgroundColor,
      dividerColor: darkPrimaryColor,
      hintColor: darkHintColor,
      focusColor: darkPrimaryColor,
      primaryColorDark: darkPrimaryTextColor,
      colorScheme: const ColorScheme.dark(
        primary: darkPrimaryTextColor,
        error: Colors.red,
      ),
      fontFamily: GoogleFonts.dmSans().fontFamily,
      appBarTheme: AppBarTheme(
        backgroundColor: darkBackgroundColor,
        elevation: 0.0,
        centerTitle: true,
        shadowColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: darkPrimaryTextColor,
          size: 28.0,
        ),
        titleTextStyle: TextStyle(
          fontSize: 16.0,
          color: darkPrimaryTextColor,
          fontFamily: GoogleFonts.dmSans().fontFamily,
          fontWeight: FontWeight.w700,
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(
              (states) => darkPrimaryTextColor),
          side: MaterialStateProperty.resolveWith(
            (states) => const BorderSide(
              width: 2.0,
              color: darkPrimaryTextColor,
            ),
          ),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.dmSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith(
              (states) => darkPrimaryTextColor.withOpacity(0.3)),
          elevation: MaterialStateProperty.resolveWith((states) => 0),
          fixedSize: MaterialStateProperty.resolveWith(
            (states) => const Size.fromHeight(52.0),
          ),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith(
            (states) => darkPrimaryTextColor,
          ),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.dmSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          foregroundColor: MaterialStateProperty.resolveWith(
              (states) => darkPrimaryTextColor),
          elevation: MaterialStateProperty.resolveWith((states) => 0),
          fixedSize: MaterialStateProperty.resolveWith(
            (states) => const Size.fromHeight(52.0),
          ),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(
            (states) => darkPrimaryTextColor,
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.dmSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith(
              (states) => const Color(0xFF0A0913).withOpacity(0.4)),
        ),
      ),
      chipTheme: ChipThemeData(
        backgroundColor: darkPrimaryTextColor,
        disabledColor: Colors.grey[300]!,
        selectedColor: darkPrimaryTextColor,
        secondarySelectedColor: Colors.white30,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
        labelStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w600,
        ),
        secondaryLabelStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w600,
        ),
        brightness: Brightness.light,
      ),
      iconTheme: const IconThemeData(
        size: 20.0,
        color: darkPrimaryTextColor,
      ),
      radioTheme: RadioThemeData(
        fillColor:
            MaterialStateProperty.resolveWith((states) => darkPrimaryTextColor),
      ),
      dialogTheme: DialogTheme(
        backgroundColor: darkPrimaryTextColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
        elevation: 0.0,
        titleTextStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w700,
          fontSize: 18.0,
        ),
        contentTextStyle: GoogleFonts.dmSans().copyWith(
          height: 1.4,
        ),
      ),
      tabBarTheme: TabBarTheme(
        labelStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w600,
          fontSize: 13.0,
        ),
        unselectedLabelStyle: GoogleFonts.dmSans().copyWith(
          fontWeight: FontWeight.w600,
          fontSize: 13.0,
        ),
        indicator: BoxDecoration(
          color: darkPrimaryTextColor,
          borderRadius: BorderRadius.circular(16.0),
        ),
        labelColor: Colors.white,
        unselectedLabelColor: darkPrimaryTextColor,
      ),
      textTheme: TextTheme(
        headline3: GoogleFonts.dmSans().copyWith(
          color: darkPrimaryTextColor,
        ),
        headline5: GoogleFonts.dmSans().copyWith(
          color: darkPrimaryTextColor,
          fontWeight: FontWeight.bold,
        ),
        headline6: GoogleFonts.dmSans().copyWith(
          color: darkPrimaryTextColor,
        ),
        subtitle1: GoogleFonts.dmSans().copyWith(
          color: darkPrimaryTextColor,
        ),
        bodyText1: GoogleFonts.dmSans().copyWith(
          color: darkPrimaryTextColor,
        ),
        bodyText2: GoogleFonts.dmSans().copyWith(
          color: darkPrimaryTextColor,
        ),
        button: GoogleFonts.dmSans().copyWith(
          color: darkPrimaryTextColor,
        ),
        caption: GoogleFonts.dmSans().copyWith(
          color: darkPrimaryTextColor,
        ),
      ),
    );
  }
}
